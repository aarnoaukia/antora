'use strict'

const commander = require('commander')
require('./commander/options-from-convict')

module.exports = commander
